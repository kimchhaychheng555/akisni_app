'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/AssetManifest.json": "70a508f47e77c3d489cd86b13a4ca909",
"assets/assets/fonts/Kh%2520Ang%2520Writehand.ttf": "1b459d9217f03164feb38ae86389caab",
"assets/assets/fonts/Maven_Pro/MavenPro-Black.ttf": "d5d599f78e49e566e1db644f01d61194",
"assets/assets/fonts/Maven_Pro/MavenPro-Bold.ttf": "8a8f30e7b8504641f217f7605d649c3a",
"assets/assets/fonts/Maven_Pro/MavenPro-ExtraBold.ttf": "d2cd883ced3ca3413ebbdc154256e7f4",
"assets/assets/fonts/Maven_Pro/MavenPro-Medium.ttf": "050cae3121b1fb72518b89c15122ff10",
"assets/assets/fonts/Maven_Pro/MavenPro-Regular.ttf": "d53070b8d8479a7080748d18a06cd4cd",
"assets/assets/fonts/Maven_Pro/MavenPro-SemiBold.ttf": "2676d4321900217712ff3ada4df262a3",
"assets/assets/images/197505.png": "786fe458cbfa5919286b066e09eb48c4",
"assets/assets/images/en_flage.png": "f5d6a4508f62dbfe329f75e7a72ba7f4",
"assets/assets/images/icon.png": "3d036b1648d82cc2bf93d95fbc9658d5",
"assets/assets/images/image%25201.png": "b85905c5538d46dc734a82d0649e4916",
"assets/assets/images/khmer_flage.png": "6ab5f28e10eb6f51fa5f64d0ea77385a",
"assets/assets/images/location.png": "a6ee8b0229327b3f193421f5f26e9ebd",
"assets/assets/images/LOGO%2520AKISNI-01.jpg": "31beb86df39f7ea283713b9052cdf75d",
"assets/assets/images/logo.png": "8b44bf8ed3d214c2e43e9b99fb4ed141",
"assets/assets/images/placeholder-image.jpg": "8865f01b09e3902c0a7c342fa11a0234",
"assets/assets/images/profile_1.jpg": "b88e4682b2833f6e2c132b4b88923645",
"assets/assets/images/profile_2.jpg": "7b3b042913ebf47f0ac6114b132a9f83",
"assets/assets/images/profile_3.jpg": "29b2f4e6695266996a1092b0414ee5fc",
"assets/assets/images/Rectangle%2520242.png": "37ff191c244ec3729e29e8b96e2287c2",
"assets/assets/images/tower.png": "051dad2f0ea023bfacde24eb9aec117b",
"assets/assets/images/user.png": "f3b485a08290f54af963013039f3cfd8",
"assets/assets/svg/location.svg": "0054bea92d97a3718777111fa5df533c",
"assets/assets/svg/LOGO%2520AKISNI-01.jpg": "31beb86df39f7ea283713b9052cdf75d",
"assets/FontManifest.json": "e8170b1dc4af8fa6fab55c043f30c415",
"assets/fonts/MaterialIcons-Regular.otf": "95db9098c58fd6db106f1116bae85a0b",
"assets/NOTICES": "1d529f7443814245f907d361cfc2d79e",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/fluttertoast/assets/toastify.css": "a85675050054f179444bc5ad70ffc635",
"assets/packages/fluttertoast/assets/toastify.js": "e7006a0a033d834ef9414d48db3be6fc",
"assets/shaders/ink_sparkle.frag": "92514212ce53a9136d5fcd4f9279b044",
"canvaskit/canvaskit.js": "2bc454a691c631b07a9307ac4ca47797",
"canvaskit/canvaskit.wasm": "bf50631470eb967688cca13ee181af62",
"canvaskit/profiling/canvaskit.js": "38164e5a72bdad0faa4ce740c9b8e564",
"canvaskit/profiling/canvaskit.wasm": "95a45378b69e77af5ed2bc72b2209b94",
"favicon.png": "3d036b1648d82cc2bf93d95fbc9658d5",
"flutter.js": "f85e6fb278b0fd20c349186fb46ae36d",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"index.html": "4b9486a5899d2118724c43862ef7a63b",
"/": "4b9486a5899d2118724c43862ef7a63b",
"main.dart.js": "4bc95d361c7deca338167a7d305f6337",
"manifest.json": "6f69ec203d1308ebf22aeeaa3edb7a07",
"version.json": "87660789681b9fd3ca204fc3faee940d"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "main.dart.js",
"index.html",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
